# Research Project 2, Semester 1 2018 #

What are the existing methods for estimating power to detect a difference in correlations between identical (monozygotic) and non-identical (dizygotic) twins, how do these compare and can they be improved upon?  This question will be addressed through a literature review, and comparison of methods using both theory and simulation.

### What is this repository for? ###

This repository is intended to contain some of the main project documentation - admin files for context and planning, script files for analyses, and report drafts.

Carl Higgs 6 March 2018
higgsc@student.unimelb.edu.au